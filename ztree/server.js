let express = require('express')
const cors = require('cors')
let mock = require('mockjs')
const bodyParser = require('body-parser')
const router = require('./router')

let app = express()

app.use(cors());
app.use(bodyParser())

app.use('/', router)



/**
 * 监听8091端口
 */
app.listen('8090');
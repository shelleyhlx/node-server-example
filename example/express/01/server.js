var express = require('express')
//引入express框架，通过此框架快速开发get/post接口

var bodyParser = require('body-parser')
//引入body-parser中间件，用于解析接口

var app = express();

// 编写get接口
// http://localhost:9076/get?name=3
app.get('/get', function (request, response){
  console.log(request.originalUrl, request.query, request.url)
  // /get?name=3 { name: '3' } /get?name=3
  let data = { 'name':'abc', 'value':'123' };
  response.json(data);
})

// jsonp 接口
app.get('/jsonp', function (request, response){
  console.log(request.query)
  let data = {
    message: '请求成功',
    code: '200',
    data: {
      name: request.query.name,
      age: 11,
      req: '请求成功',
    }
  }
  data = JSON.stringify(data)
  response.end('func('+data+')')
})

app.use(bodyParser.json());
// 添加bodyParser中间件，通过这一命令可实现对
// post中提交的application/json数据格式参数进行解析

app.post('/post', function (request, response){
  var name = request.body.name;
  var data = {
    'data': {
      'name': name,
      'sex': '0'
    }
  }
  response.json(data);
})

var server = app.listen(9076, function (){
  var host = server.address()
  console.log(host)
})

// localhost:9076/get?name=3






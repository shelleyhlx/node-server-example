let express = require('express')
const cors = require('cors')
let mock = require('mockjs')
const bodyParser = require('body-parser')
const router = require('./router')

let app = express()

// app.use(cors());
app.use(bodyParser())

/*为app添加中间件处理跨域请求 和 app.use(cors()) 一个作用 */
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
  // res.header('Access-Control-Allow-Headers', 'Content-Type, Content-Length, Authorization, Accept, X-Requested-With , yourHeaderFeild');
  res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  next();
});
app.use('/', router)
/**
 * mockjs中属性名‘|’符号后面的属性为随机属性，
 * 数组对象后面的随机属性为随机数组数量，即个数
 * 正则表达式表示随机规则，
 * +1代表自增
 */
// 发现我以前看的是鬼文档啊。
// https://github.com/nuysoft/Mock/wiki/Getting-Started
// https://github.com/nuysoft/Mock/wiki/Syntax-Specification
/**
 * // 使用 Mock
 var Mock = require('mockjs')
 var data = Mock.mock({
    // 属性 list 的值是一个数组，其中含有 1 到 10 个元素
    'list|1-10': [{
        // 属性 id 是一个自增数，起始值为 1，每次增 1
        'id|+1': 1
    }]
})
 // 输出结果
 console.log(JSON.stringify(data, null, 4))
 */
/**
 {"msg":"","status":200,"data":[{"name":"yCQMJId","id":1,"value":3651},{"name":"UReaWvg","id":2,"value":3995},{"name":"foOqwtm","id":3,"value":3072},{"name":"GsisGk","id":4,"value":294},{"name":"MyYvL","id":5,"value":552},{"name":"tPsStN","id":6,"value":3096},{"name":"lKTwaZo","id":7,"value":3726},{"name":"LeqkUM","id":8,"value":3223},{"name":"qKGwh","id":9,"value":2951},{"name":"JwJFeH","id":10,"value":1534}]}
 */
// data|10 10 条数据



/**
 * 监听8090端口
 */
app.listen('8090');
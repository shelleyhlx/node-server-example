const express = require("express");
const router = express.Router();
const mock = require("mockjs");

router.get("/open", function (req, res, next) {
  res.json(mock.mock({
    'message': '请求成功',
    'code': '0',
    'result': {
      'list|20': [{
        'id|+1': 1,
        'name': '@city',
        'mode|1-2': 1,
        'op_mode|1-2': 1,
        'franchisee_id|': 77,
        'franchisee_name|': '松果自营',
        'city_admins|1-2': [{
          'user_name': '@cname',
          'user_id|+1': 1001,
        }],
        'open_time': '@datetime',
        'update_time': 1645494438486,
        'sys_user_name': '@cname',
      }],
      page: 1,
      pageSize: 4,
      total_count: 100,
      page_count: 6,
    }
  }))
});

module.exports = router;


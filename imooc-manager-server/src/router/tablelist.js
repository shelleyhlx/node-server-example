const express = require("express");
const router = express.Router();
// const Result = require("../model/Result");
// const { login } = require("../services/user");
// const { md5 } = require("../utils");
// const { PWD_SALT } = require("../utils/constant");
const mock = require("mockjs");
const {cname} = require("mockjs/src/mock/random/name");
/**
 * req: request，浏览器 向 服务器 发送的请求，包括数据
 * res：response，服务端 发送到 浏览器 的响应，包括数据
 * 拿到req的数据，处理，res.json 发送到 浏览器
 */
// router.post("/login", function (req, res) {
//   console.log("/uesr/login", req.body);
//   let { username, password } = req.body;
//   // 加密
//   password = md5(`${password}${PWD_SALT}`);
//   login(username, password).then((user) => {
//     if (!user || user.length === 0) {
//       new Result("登录失败").fail(res);
//     } else {
//       new Result("登录成功").success(res);
//     }
//   });
// });

// http://localhost:5000/table/list
router.get("/list", function (req, res, next) {
  res.json(mock.mock({
    'message': '请求成功',
    'code': '0',
    'result': {
      'list|20': [{
        'id|+1': 1,
        'userName': '@cname',
        'sex|1-2': 1,
        'state|1-5': 1,
        'interest|1-6': 1,
        'birthday': '2000-2-2',
        'address': '广州',
        'time': '04:00:00',
        'time2': '04:00:00',
        'time3': '05:00:00',
        'time4': '06:00:00',
      }],
      page: 1,
      pageSize: 4,
      total: 100,
    }
  }))
});
// http://localhost:8090/table/high
router.get("/high", function (req, res, next) {
  res.json(mock.mock({
    'message': '请求成功',
    'code': '0',
    'result': {
      'list|20': [{
        'id|+1': 1,
        'userName': 'cname',
        'sex|1-2': 1,
        'age|10-40': 19,
        'state|1-5': 1,
        'interest|1-6': 1,
        'birthday': '2000-2-2',
        'address': '广州',
        'time': '04:00:00',
        'time2': '04:00:00',
        'time3': '05:00:00',
        'time4': '06:00:00',
      }],
      page: 1,
      pageSize: 4,
      total: 100,
    }
  }))
});

module.exports = router;
const express = require("express");
const boom = require("boom");
const tablelistRouter = require("./tablelist");
const cityRouter = require('./city')
const orderRouter = require('./order')
const userRouter = require('./user')
const { CODE_ERROR } = require("../utils/constant");

// 注册路由
const router = express.Router();

// router.get("/", function (req, res) {
//   res.send("小慕读书后台");
// });

router.use("/table", tablelistRouter);
router.use('/city', cityRouter);
router.use('/order', orderRouter)
router.use('/user', userRouter)
/**
 * 集中处理404请求的中间件
 * 注意：该中间件必须放在正常处理流程之后
 * 否则，会拦截正常请求
 * http://localhost:5000/bb 页面显示 Error: 接口不存在
 */
router.use((req, res, next) => {
  next(boom.notFound("接口不存在"));
});

/**
 * 自定义路由异常处理中间件
 * 注意两点：
 * 第一，方法的参数不能减少
 * 第二，方法的必须放在路由最后
 * http://localhost:5000/bb
 * {"code":-1,"msg":"接口不存在","error":404,"errorMsg":"Not Found"} 404
 * 方便前端处理
 */
router.use((err, req, res, next) => {
  console.log(err); // 执行的 cmd 处输出
  const msg = (err && err.message) || "系统错误";
  const statusCode = (err.output && err.output.statusCode) || 500;
  const errorMsg =
    (err.output && err.output.payload && err.output.payload.error) ||
    err.message;
  res.status(statusCode).json({
    code: CODE_ERROR,
    msg,
    error: statusCode,
    errorMsg,
  });
});

module.exports = router;